---
title: "Careers"
date: 2020-04-25T13:22:09-04:00
menu: "main"
weight: 20
---

We are always looking for new team members!  Francois' Sushi provides a fast paced environment for the eager go-getter.  If you're good at removing obstacles, especially when nobody is looking, you'll find a home here.

## Requirements

* Commitment to see the objective eliminated.
* Looking for a true Sushi Ninja!
* Strong knife skills a must.
* No criminal convictions.

## Nice to Have

* Clean driving record.
* References from past sensei.
* 1st dan or equivalent.
* Own knives or brass knuckles.

## Benefits

* Uniforms and dojo space provided.
* Legal Representation.
* Funeral Services.
