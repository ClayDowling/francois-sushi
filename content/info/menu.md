---
title: "Menu"
date: 2020-04-25T14:27:37-04:00
draft: false
menu: "main"
weight: 10
---

## No. 1 Heartbreak Roll

Tuna roll with bitter melon.  Unfertilized salmon roe.

Please include name, address, and known associates of former lover.  75% upcharge to include the home-wrecking tramp they left you for.

## No. 2 Chauvenist Pigs In A Blanket

Finely chopped sausage with jalapeno.  Served flaming.

Please include photo w/ fedora.  For an extra $50 you can assist in preparation.

## No. 3  S.S. Tanya Harding Sushi Boat

Up to five different rolls.  Kneecaps a specialty.  During cheer season please call one day ahead.
