---
title: "Grand Opening"
date: 2020-04-25T15:15:17-04:00
---

Francois' Sushi is Ann Arbor's newest delivery-only restaurant.  Francois' staff understands the stresses of modern life, environmental disaster, and political upheaval can make it hard to provide nutritious, peaceful meals for your family.  Francois' team of sushi ninjas will remove the stress from every day life and provide a fresh, nutritious meal in 60 minutes or less.

Our sushi-ninjas provide an unparalleled level of service.  Permanent removal or your problems guaranteed or your money back!

Call us about our party platters.  Parties of 5 or more please call 24 hours in advance.

*Order By Phone:* (734) 555-5309

{{< figure src="/img/host-darcy.jpg" caption="Host D'Arcy is waiting to provide you with a dining experience free of rodents and small dogs." >}}